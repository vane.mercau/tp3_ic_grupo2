from typing import List

class Estado:
    CRUZ = 1
    CIRCULO = 0
    VACIO = -1
    EMPATE = 2

class TatetiGeneralizado :

    # Tamanio del tablero n x n y condicion ganadora k
    def __init__(self, n: int, k: int) -> None:
        # PRE: { n >= 1 and k >= 1 and n >= k }

        # Estructura de representacion
        self._n: int = n
        self._k: int = k
        self._cruces: int = 0
        self._circulos: int = 0
        self._casilleros_libres: int = n*n
        self._termino: bool = False
        self._ganador: int = Estado.VACIO
        self._jugador: int = Estado.CRUZ
        self._tablero: List[ List[int] ] = self._nuevo_tablero()


    def _nuevo_tablero(self) -> List[ List[int] ]:

        tablero_vacio = []
        for i in range(self._n):
            tablero_vacio.append( [Estado.VACIO] * self._n )        
        return tablero_vacio
        
    
    def _k_fichas_juntas_vertical(self, pos_x: int, pos_y: int, jugador: int) -> bool:

        i: int = max(0, pos_x - self._k + 1)
        j: int = pos_y
        
        max_pos_i: int = min( self._n - 1, pos_x + self._k - 1 )

        contador: int = 0
        while i <= max_pos_i and contador < self._k:
            if self._tablero[i][j] == jugador:
                contador += 1
            else:
                contador = 0
            i += 1

        return (contador == self._k)


    def _k_fichas_juntas_horizontal(self, pos_x: int, pos_y: int, jugador: int) -> bool:

        i: int = pos_x
        j: int = max(0, pos_y - self._k + 1)
        
        max_pos_j: int = min( self._n - 1, pos_y + self._k - 1 )

        contador: int = 0
        while j <= max_pos_j and contador < self._k:
            if self._tablero[i][j] == jugador:
                contador += 1
            else:
                contador = 0
            j += 1

        return (contador == self._k)


    def _k_fichas_juntas_diagonal(self, pos_x: int, pos_y: int, jugador: int) -> bool:

        i: int = max( 0, pos_x - (self._k - 1) )
        j: int = max( 0, pos_y - (self._k - 1) )
        
        max_pos_i: int = min( self._n - 1, pos_x + self._k - 1 )
        max_pos_j: int = min( self._n - 1, pos_y + self._k - 1 )

        contador: int = 0
        while i <= max_pos_i and j <= max_pos_j and contador < self._k:
            if self._tablero[i][j] == jugador:
                contador += 1
            else:
                contador = 0
            i += 1
            j += 1
        
        return (contador == self._k)


    def _termino_con_jugada(self, pos_x: int, pos_y: int, jugador: int) -> None:
        
        if self._k_fichas_juntas_horizontal(pos_x, pos_y, jugador) or \
                self._k_fichas_juntas_vertical(pos_x, pos_y, jugador) or \
                self._k_fichas_juntas_diagonal(pos_x, pos_y, jugador):
            
            self._ganador = jugador
            self._termino = True
        
        elif self._casilleros_libres == 0:
            self._ganador = Estado.EMPATE
            self._termino = True


    def jugada(self, pos_x: int, pos_y: int) -> None:
        # PRE: { 0 <= pos_x < _n and 0 <= pos_y < _n and _tablero[pos_x][pos_y] == Estado.VACIO and _ganador == Estado.VACIO }
        self._casilleros_libres -= 1
        
        if self._jugador == Estado.CRUZ:
            self._cruces += 1
            self._jugador = Estado.CIRCULO
            self._tablero[pos_x][pos_y] = Estado.CRUZ
            
            if self._cruces >= self._k:
                self._termino_con_jugada(pos_x, pos_y, Estado.CRUZ)
        
        else:
            self._circulos += 1
            self._jugador = Estado.CRUZ
            self._tablero[pos_x][pos_y] = Estado.CIRCULO
            
            if self._circulos >= self._k:
                self._termino_con_jugada(pos_x, pos_y, Estado.CIRCULO)
   

    def turno(self) -> str:

        if self._jugador == Estado.CRUZ:
            return "Es el turno de Cruz"
        else:
            return "Es el turno de Circulo"


    def cruces(self) -> int:

        return self._cruces
    

    def circulos(self) -> int:

        return self._circulos


    def casilleros_libres(self) -> int:

        return self._casilleros_libres


    def termino(self) -> bool:

        return  (self._ganador != Estado.VACIO)
    

    def ganador(self) -> str:

        if self._ganador == Estado.CRUZ:
            return "Gano Cruz"
        elif self._ganador == Estado.CIRCULO:
            return "Gano Circulo"
        elif self._ganador == Estado.EMPATE:
            return "Empate"
        else:
            return "El juego no termino"


    def __str__(self) -> str:
        # buscamos el siguiente formato de impresion del tablero
        # 
        # +---+---+
        # | o | x |
        # +---+---+
        # |   | x |
        # +---+---+
        
        rv: str = ""
        
        k: int = 0
        aux: str = ""
        while k < self._n:
            aux += "+---"
            k += 1
        aux += "+\n"

        i: int = 0
        j: int = 0
        while j < self._n:
            i = 0
            rv += aux
            while i < self._n:
                rv += "|"
                if self._tablero[j][i] == Estado.CIRCULO:
                    rv += " o "
                elif self._tablero[j][i] == Estado.CRUZ:
                    rv += " x "
                else:
                    rv += "   "
                i += 1
            rv += "|\n"
            j += 1
        rv += aux

        return rv.rstrip()
